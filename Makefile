
CFLAGS = $(shell apxs2 -q CPPFLAGS)
CFLAGS += -I$(shell apxs2 -q INCLUDEDIR)
CFLAGS += $(shell pkg-config --silence-errors --cflags apr-1 || true)
CFLAGS += -O2
LDFLAGS = $(shell apxs2 -q LDFLAGS)

all: suexec

install: all
	install -m 755 -o root -g root -d $(DESTDIR)/usr/lib/apache2
	install -m 4754 -o root -g www-data -s suexec $(DESTDIR)/usr/lib/apache2/suexec
	#install -m 755 -o root -g root -s php.fcgi $(DESTDIR)/usr/lib/cgi-bin/php.fcgi
	#install -m 755 -o root -g root -s php5.6.fcgi $(DESTDIR)/usr/lib/cgi-bin/php5.6.fcgi

clean:
	-rm -f suexec php.fcgi php5.6.fcgi

suexec: suexec.c suexec.h
	$(CC) $(LDFLAGS) $(CFLAGS) -o $@ $<

php.fcgi: php.fcgi.c
	$(CC) -DPHPBIN=\"/usr/lib/cgi-bin/php\" -Os -o $@ $^

php5.6.fcgi: php.fcgi.c
	$(CC) -DPHPBIN=\"/usr/lib/cgi-bin/php5.6\" -Os -o $@ $^
